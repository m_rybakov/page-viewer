
angular
    .module('pageViewer', []);

angular.module('pageViewer').run(['$templateCache', function($templateCache) {
    $templateCache.put('page-viewer.directive.html',
        "\r\n<style>page-viewer {\n  display: block; }\n\n.lgs-viewport-wrapper {\n  width: 100%;\n  height: 100%;\n  position: relative;\n  background-color: #f7f7f9;\n  border: 1px solid #e1e1e8;\n  border-radius: 4px;\n  user-select: none; }\n\n.lgs-viewport-controls {\n  top: 10px;\n  left: 10px; }\n\n.lgs-viewport-overlays, .lgs-viewport-controls {\n  position: absolute; }\n\n.lgs-viewport, .lgs-viewport-overlays {\n  height: 100%;\n  width: 100%; }\n\n.lgs-viewport-overlays h3 {\n  top: 50%;\n  position: absolute;\n  margin: 0;\n  width: 100%;\n  transform: translate(0, -50%); }\n\n.lgs-page-dragging {\n  cursor: pointer;\n  cursor: -webkit-grab; }\n</style>\r\n\r\n\r\n<div class=\"lgs-viewport-wrapper\">\r\n    <div class=\"lgs-viewport-controls\">\r\n        <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\r\n            <button type=\"button\" class=\"btn btn-default\" ng-click=\"zoomIn()\">\r\n                <span class=\"glyphicon glyphicon-zoom-in\"></span>\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-default\" ng-click=\"fit()\">\r\n                <span class=\"glyphicon glyphicon-resize-full\"></span>\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-default\" ng-click=\"zoomOut()\">\r\n                <span class=\"glyphicon glyphicon-zoom-out\"></span>\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-default\" ng-show=\"image\">\r\n                <span ng-bind=\"scale + '%'\"></span>\r\n            </button>\r\n        </div>\r\n\r\n        <div class=\"btn-group\" role=\"group\">\r\n            <button class=\"btn btn-default\" type=\"button\" ng-click=\"rotateLeft()\">\r\n                <i class=\"fa fa-undo\"></i>\r\n            </button>\r\n            <button class=\"btn btn-default\" type=\"button\" ng-click=\"rotateRight()\">\r\n                <i class=\"fa fa-repeat\"></i>\r\n            </button>\r\n        </div>\r\n    </div>\r\n    <div class=\"lgs-viewport-overlays text-center\" ng-hide=\"image\">\r\n        <h3 ng-show=\"noData\" class=\"text-muted\">No data</h3>\r\n        <h3 ng-show=\"loading\" class=\"text-muted\">Loading</h3>\r\n    </div>\r\n    <canvas class=\"lgs-viewport\" resize=\"true\" ng-class=\"{true: 'lgs-page-dragging'}[mousePressed]\" data-girar=\"0\"></canvas>\r\n    <img ng-src=\"{{image.src}}\" height=\"{{image.height}}\" width=\"{{image.width}}\" style=\"display: none\">\r\n</div>");
}]);

angular
    .module('pageViewer')
    .directive('pageViewer', imageViewportDirective);

/**
 * @ngInject
 */
function imageViewportDirective($compile, $window) {

    var LABELS = {
        zoomIn: 'Zoom In',
        zoomOut: 'Zoom Out',
        rotateRight: 'Rotate Right',
        rotateLeft: 'Rotate Left',
        fitContainer: 'Fit to container',
        actualSize: 'Actual Size',
        noData: 'No data'
    };

    return {
        restrict: 'E',
        scope: {
            image: '=page',
            overlays: '=',
            options: '=viewerOptions'
        },
        templateUrl: 'page-viewer.directive.html',
        link: link
    };



    function link (scope, element, attrs) {

        var image = element.find('img'),
            view = null,
            page = null,

            delta = {
                zoomIn: 1.1,
                zoomOut: 0.9
            };

        var zoom = { min: null, max: 2 };

        /** Scope values **/

        scope.isLoading = false;
        scope.noData = true;
        scope.zoomLevel = null;


        /** Watchers **/

        //Forced scope evaluation after window resize
        angular.element($window).bind('resize', function () {
            scope.$apply();
        });

        scope.$watch('overlays', function () {

            if (!scope.overlays) {
                return;
            }

            var selected = scope.overlays.filter(function (index) {
                return index.selected;
            });

            selected = selected.length ? selected[0] : null;

            if (!selected) {
                return;
            }

            page.fitBounds(view.bounds);
            zoom.min = page.scaling.x;


            var size = new paper.Size(selected.location.width, selected.location.height)
                .multiply(page.scaling),

                point = new paper.Point([
                    selected.location.x + selected.location.width / 2,
                    selected.location.y + selected.location.height / 2
                ]).subtract(new paper.Point(page.width / 2, page.height / 2));

            point = page.localToGlobal(point);
            page.translate(page.bounds.center.subtract(point));

            //Calculate correct scale for portrait or landscape orientation
            var scale = Math.round(page.rotation) / 90 % 2 !== 0 ?
                Math.min(view.size.width, view.size.height) / Math.max(size.width, size.height) :
                Math.max(view.size.width, view.size.height) / Math.max(size.width, size.height);

            //Take 90%
            scale *= 0.9;

            //Limit zoom
            if (page.scaling.x * scale > zoom.max) {
                scale = zoom.max / page.scaling.x;
            }

            page.scale(scale, view.bounds.center);
            scope.scale = Math.round(page.scaling.x * 100);

            /**
             * Correct page translation after it's scale
             */
            correctZoomTranslation();

            view.update();

        }, true);


        //Watching data source
        scope.$watch('image', function () {
            if (!scope.image) {
                page = null;
                if (view) {
                    paper.project.clear();
                    view.update();
                    view.remove();
                    view = null;
                }
            }
        });


        image.on('load', init);

        function init () {

            paper.setup(element.find('canvas')[0]);
            view = paper.view;

            page = new paper.Raster(image[0], view.center);
            page.fitBounds(view.bounds);

            zoom.min = page.scaling.x;

            view.update();

            page.onMouseDrag = onMouseDrag;
            
            page.onMouseDown = function () {
                scope.$apply(scope.mousePressed = true);
            };

            page.onMouseUp = function () {
                scope.$apply(scope.mousePressed = false);
            };

            view.onResize = function (e) {
                if (view) {
                    page.fitBounds(view.bounds);
                    zoom.min = page.scaling.x;
                    page.position = view.center;
                }
            };


            scope.$apply(function () {
                scope.scale = Math.round(page.scaling.x * 100);
            });


            angular.element(view.element)
                .off('mousewheel DOMMouseScroll')
                .on('mousewheel DOMMouseScroll', onMouseWheel);

        }


        /** Functions **/

        scope.zoomIn = function () {
            if (page) {

                var delta = 1.1;
                if (page.scaling.x * delta < zoom.max) {
                    page.scale(delta, view.bounds.center);
                } else {
                    page.scaling = new paper.Point(zoom.max, zoom.max);
                }
                scope.scale = Math.round(page.scaling.x * 100);
                correctZoomTranslation();

                view.update();
            }
        };

        scope.zoomOut = function () {
            if (page) {
                var delta = 0.9;
                if (page.scaling.x * delta >= zoom.min) {
                    page.scale(delta, view.bounds.center);
                } else {
                    page.fitBounds(view.bounds);
                }
                scope.scale = Math.round(page.scaling.x * 100);
                correctZoomTranslation();

                view.update();
            }
        };

        scope.rotateLeft = function () {
            if (page) {
                page.rotate(-90);
                page.fitBounds(view.bounds);
                zoom.min = page.scaling.x;
                view.update();
            }
        };

        scope.rotateRight = function () {
            if (page) {
                page.rotate(90);
                page.fitBounds(view.bounds);
                zoom.min = page.scaling.x;
                view.update();
            }
        };

        scope.fit = function () {
            if (page) {
                page.fitBounds(view.bounds);
                zoom.min = page.scaling.x;
                scope.scale = Math.round(page.scaling.x * 100);
                view.update();
            }
        };


        /** Event handlers **/


        function onMouseDrag(e) {

            var translation = new paper.Point(e.delta),

                //Translated page
                translated = new paper.Rectangle({
                    point: page.bounds.point.add(translation),
                    size: page.bounds.size
                });


            var visiblePagePart = view.bounds.intersect(translated);

            if (visiblePagePart.width < view.bounds.width) {

                if (view.size.width > page.bounds.width) {
                    translation.x = 0;
                    page.position.x = view.center.x;
                } else {
                    translation.x = 0;
                }
            }

            if (visiblePagePart.height < view.bounds.height) {

                if (view.size.height > page.bounds.height) {
                    translation.y = 0;
                    page.position.y = view.center.y;
                } else {
                    translation.y = 0;
                }

            }

            page.translate(translation);
        }


        function onMouseWheel(e) {

            var delta = (e.deltaY || e.wheelDelta || e.detail) > 0 ? 1.1 : 0.9,
                zoomPoint = new paper.Point(e.offsetX, e.offsetY);

            zoomPoint = zoomPoint.isInside(page.bounds) ? zoomPoint : page.bounds.center;


            if (view.bounds.width >= page.bounds.width) {
                zoomPoint.x = view.center.x;
            }

            if (view.bounds.height >= page.bounds.height) {
                zoomPoint.y = view.center.y;
            }


            if (page.scaling.x * delta < zoom.min) {
                page.fitBounds(view.bounds);
            } else if (page.scaling.x * delta < zoom.max) {
                page.scale(delta, zoomPoint);
            } else {
                page.scaling = new paper.Point(zoom.max, zoom.max);
            }

            correctZoomTranslation();
            scope.$apply(scope.scale = Math.round(page.scaling.x * 100));
            view.update();

        }


        /**
         * Correct page position after zoom
         */
        function correctZoomTranslation () {
            var visiblePartOfPage = view.bounds.intersect(page.bounds);
            if (visiblePartOfPage.width != page.bounds.width || visiblePartOfPage.height != page.bounds.height) {
                var shift = view.bounds.center
                    .subtract(visiblePartOfPage.center)
                    .multiply(2); //не понятно почему на 2, но работает
                page.translate(shift);
            }

            if (page.bounds.width <= view.size.width) {
                page.position.x = view.center.x;
            }

            if (page.bounds.height <= view.size.height) {
                page.position.y = view.center.y;
            }
        }

    }
}