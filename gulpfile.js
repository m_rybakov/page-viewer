
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    inject = require('gulp-inject'),
    sass = require('gulp-sass'),
    smoosher = require('gulp-smoosher'),
    clean = require('gulp-clean'),
    ngTemplate = require('gulp-angular-templates');

var src = {
    js: [
        'src/*.module.js',
        'src/**.html.js',
        'src/*.directive.js'
    ],
    styles: [
        'src/**.sass'
    ],
    template: [
        'src/**.html'
    ]
};

gulp.task('default', ['directive'], function () {
    gulp.watch('./src/**', ['directive']);
});

gulp.task('directive', ['template'], function () {
    return gulp.src(src.js)
        .pipe(concat('page-viewer.directive.js'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('template', ['styles'], function () {
    return gulp.src(src.template)
        .pipe(smoosher())
        .pipe(ngTemplate({
            module: 'pageViewer'
        }))
        .pipe(gulp.dest('./src'))
});

gulp.task('styles', function () {
    return gulp.src(src.styles)
        .pipe(sass())
        .pipe(gulp.dest('./src'))
});

gulp.task('clean', ['template'], function () {
    return gulp.src([
        './src/**.css',
        './src/**.html.js'
    ], {read: false}).pipe(clean());
});