angular
    .module('viewportDemoApp', ['pageViewer'])
    .controller('DemoController', ['$timeout', function ($timeout) {

        var vm = this,

            pages = [

            {
                name: 'Альфа 1',
                src: 'assets/1.png',
                indexes: [
                    {
                        name: "InvoiceNumber",
                        value: "03432/118/035313",
                        location: {
                            x: 1333,
                            y: 1004,
                            width: 437,
                            height: 85
                        }
                    }
                ]
            },

            {
                name: 'Альфа 2',
                src: 'assets/2.png',
                indexes: [
                    {
                        name: "InvoiceNumber",
                        value: "03432/118/010916",
                        location: {
                            x: 1333,
                            y: 1004,
                            width: 437,
                            height: 85
                        }
                    },
                    {
                        name: 'TotalCount',
                        value: '2587.85',
                        location: {
                            x: 1871,
                            y: 2349,
                            width: 326,
                            height: 112
                        }
                    }
                ]
            },

            {
                name: 'Kofax Sample',
                src: 'assets/3.png',
                indexes: [
                    {
                        name: 'FirstName',
                        value: 'Martin',
                        location: {
                            x: 154,
                            y: 637,
                            width: 937,
                            height: 124
                        }
                    },
                    {
                        name: 'LastName',
                        value: 'Janeway',
                        location: {
                            x: 1111,
                            y: 627,
                            width: 937,
                            height: 124
                        }
                    },
                    {
                        name: 'Address',
                        value: '19 Powers Road',
                        location: {
                            x: 164,
                            y: 847,
                            width: 1907,
                            height: 124
                        }
                    }
                ]
            }
        ];

        vm.pages = angular.copy(pages);

        vm.select = function (page) {
            vm.page = page;
        };

        vm.clear = function () {
            vm.page = null;
        };

        vm.index = function (index) {
            index.selected = true;
            $timeout(function () {
                index.selected = false;
            }, 100);
        }

    }]);